# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
NojukuCom::Application.config.secret_key_base = '2f6369031ad389705a9388a7a5d779aec5ef55ba4e6c425904d17a867d726635645a3d1eb2415fbfcfa8e7453758b38777d3916422e4580a694b41528cd42192'
