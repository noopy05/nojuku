class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :spot
#  has_many   :thumb_up
  validates :title,   presence: true
  validates :content, presence: true
end
