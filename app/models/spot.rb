class Spot < ActiveRecord::Base
  has_many   :reviews, :dependent => :destroy
  belongs_to :user
  validates  :name, presence: true
  validates  :lat, numericality: true
  validates  :lng, numericality: true

  # uploaderの設定
  mount_uploader :image, ImageUploader

end
