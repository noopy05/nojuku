# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.first
30.times do |i|
  spot = user.spots.build(name: "渋谷スクランブル交差点#{i}", description: "いわずとしれた名野宿スポット。コンクリートのため少し寝心地は堅いが、周囲からの視線が心地よく、快眠を得られること間違い無し、、、！！", lat: 35.65955366039031, lng: 139.69960546281436)
  spot.save!
end
