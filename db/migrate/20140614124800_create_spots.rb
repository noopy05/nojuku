class CreateSpots < ActiveRecord::Migration
  def change
    create_table :spots do |t|

      t.integer :user_id,     null: false
      t.string :name,         null: false
      t.text :description
      t.float :lat
      t.float :lng

      t.timestamps
    end
  end
end
