class RemoveTownFromSpots < ActiveRecord::Migration
  def up
    remove_column :spots, :town
  end

  def town
    remove_column :spots, :town, :integer
  end
end
