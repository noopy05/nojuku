class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :title,    null: false
      t.text :content,    null: false
      t.integer :user_id, null: false
      t.integer :spot_id, null: false

      ## Scores
      t.integer :score_conf, null: false
      t.integer :score_safe, null: false
      t.integer :score_stf, null: false

      t.timestamps
    end
  end
end