class DeleteTownArrayFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :town_array
  end

  def down
    add_column :users, :town_array, :text
  end
end
