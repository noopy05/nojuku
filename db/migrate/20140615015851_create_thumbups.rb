class CreateThumbups < ActiveRecord::Migration
  def change
    create_table :thumbups do |t|
      t.integer :user_id, null: false
      t.integer :review_id, null: false

      t.timestamps
    end
  end
end
